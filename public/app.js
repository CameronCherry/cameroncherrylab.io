function showProjectsSublist() {
  try {
    var vsbl = document.getElementById("nav-project-sublist").style.visibility;
    console.log(vsbl);
    if ((vsbl === "visible") || (vsbl != "")) {
      // n/a
      }
    else {
      vsbl = "visible";
      document.getElementById("nav-project-sublist").style = "visibility: "+vsbl+";";
    }
  }
  catch(err) {
    // never mind, not important
  }
}

document.body.addEventListener("click", function (evt) {
  try {
    if (evt.target.id != "nav-projects-li"){
      document.getElementById("nav-project-sublist").style = "";
    }
  }
  catch(err) {
    // never mind, not important
  }
})

try {
  document.getElementById("nav-projects-li").addEventListener("click", showProjectsSublist);
  document.getElementById("nav-projects-li").addEventListener("focus", showProjectsSublist);
}
catch(err) {
  // never mind, not important
}


try {
  /* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
  // Source: https://www.w3schools.com/howto/howto_js_navbar_hide_scroll.asp
  var prevScrollpos = window.pageYOffset;
  var topMenuHeight = document.getElementById("top-menu").offsetHeight;
  window.onscroll = function() {
    // Hide nav bar
    var currentScrollPos = window.pageYOffset;
    if ((prevScrollpos > currentScrollPos) && (currentScrollPos > 100)) {
      document.getElementById("top-menu").style.top = "-"+topMenuHeight+"px";
    } else {
      document.getElementById("top-menu").style.top = "0";
    }
    prevScrollpos = currentScrollPos;

    // Hide sublist menu in nav bar
    document.getElementById("nav-project-sublist").style = "visbility: hidden;";
  }
}
catch(err) {
  // never mind, not important
}

try {
  var internalLinks = document.querySelectorAll('a[href^="#"]');
  var topMenuHeight = document.getElementById("top-menu").offsetHeight;
  for (var i = internalLinks.length-1; i >= 0; i--) {
      internalLinks[i].addEventListener("click", function() {
        var currURL = window.location.href;
        if(!currURL.endsWith(this.getAttribute("href"))) {
          window.scrollBy(0,-topMenuHeight);
        }
      },
      false);
  }
}
catch(err) {
  // never mind, not important
}

function themeToggleChange(checkbox) {
  if(checkbox.checked == true){
    // Use light theme - Gruvbox Light
    // Various colours sourced from the gruvbox light theme
    // See: https://github.com/morhetz/gruvbox
    document.body.style.setProperty("--colour-bg-1", "#f9f5d7");
    document.body.style.setProperty("--colour-bg-2", "#ebdbb2");
    document.body.style.setProperty("--colour-bg-3", "#a89984");
    document.body.style.setProperty("--colour-bg-4", "#9d0006");
    document.body.style.setProperty("--colour-bg-5", "#282828");
    document.body.style.setProperty("--colour-lighttext-contrasting", "#fbf1c7");
    document.body.style.setProperty("--colour-normaltext", "#504945");
    document.body.style.setProperty("--colour-darktext", "#fbf1c7");
    document.body.style.setProperty("--colour-lighttext", "#665c54");
    document.body.style.setProperty("--colour-accent1", "#427b58");
    document.body.style.setProperty("--colour-accent2", "#427b58");
    document.body.style.setProperty("--colour-accent3", "#076678")
    document.body.style.setProperty("--colour-accent4", "#fbf1c7");
    document.body.style.setProperty("--colour-rule", "#458588");
  }else{
    // Use dark theme - Gruvbox dark
    // Various colours sourced from the gruvbox dark theme
    // See: https://github.com/morhetz/gruvbox
    document.body.style.setProperty("--colour-bg-1", "#282828");
    document.body.style.setProperty("--colour-bg-2", "#1d2021");
    document.body.style.setProperty("--colour-bg-3", "#7c6f64");
    document.body.style.setProperty("--colour-bg-4", "#689d6a");
    document.body.style.setProperty("--colour-bg-5", "#32302f");
    document.body.style.setProperty("--colour-lighttext-contrasting", "#282828");
    document.body.style.setProperty("--colour-normaltext", "#d5c4a1");
    document.body.style.setProperty("--colour-darktext", "#282828");
    document.body.style.setProperty("--colour-lighttext", "#928374");
    document.body.style.setProperty("--colour-accent1", "#b8bb26");
    document.body.style.setProperty("--colour-accent2", "#8ec07c");
    document.body.style.setProperty("--colour-accent3", "#d79921");
    document.body.style.setProperty("--colour-accent4", "#282828");
    document.body.style.setProperty("--colour-rule", "#b8bb26");
  }
}
